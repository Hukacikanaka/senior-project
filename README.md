# README #


### What is this repository for? ###

* Quick summary - This is the SciRacer game created for the DeVry University senior project. It includes SciRacer the hover car game with a track.
* Version - Gold


### How do I get set up? ###

1. Go to https://dl.dropboxusercontent.com/u/4219962/Hover%20Car%20Build.zip to download the .zip folder

2. Unzip the folder 

3. Go to the WindowsNoEditor folder and then find the HoverCarProject.exe

4. Launch the .exe provided

Accessing the repository: 

1. Go to https://bitbucket.org/Hukacikanaka/senior-project/downloads

2. Click Download repository in the downloads section

3. Upon completion of the download you can run the project through the "HoverCarProject.uproject" file.

### Who do I talk to? ###
Team can be contacted using the DeVry site. Use the DeVry site to email everyone about any issues that may arise through the project.